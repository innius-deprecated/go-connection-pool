package pool

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestUninitializedConnectionShouldNotBeValid(t *testing.T) {
	var c *Connection = &Connection{}
	assert.False(t, c.IsValid())
}
