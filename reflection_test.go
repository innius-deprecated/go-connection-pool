package pool

import (
	"github.com/stretchr/testify/assert"
	"strings"
	"testing"
)

type foo struct {
	name string
}

type bar struct {
}

func TestAssignmentOnNilShouldFail(t *testing.T) {
	var source *foo = &foo{name: "foo"}
	var target *foo = nil
	e := assign(target, source)
	assert.NotNil(t, e)
	assert.True(t, strings.Contains(e.Error(), "nil"))
}

func TestAssignmentFromNilShouldFail(t *testing.T) {
	var source *foo = nil
	var target *foo = &foo{}
	e := assign(target, source)
	assert.NotNil(t, e)
	assert.True(t, strings.Contains(e.Error(), "nil"))
}

func TestTypesShouldMatch(t *testing.T) {
	var source *foo = &foo{name: "foo"}
	var target *bar = &bar{}
	assert.NotNil(t, assign(target, source))
	assert.NotEqual(t, source, target)
}

func TestInitializedAssignment(t *testing.T) {
	var source *foo = &foo{name: "foo"}
	var target *foo = &foo{}
	assert.Nil(t, assign(target, source))
	assert.Equal(t, source, target)
}

func TestIsNil(t *testing.T) {
	assert.True(t, is_nil(nil))
}

func TestIsNotNil(t *testing.T) {
	assert.False(t, is_nil(&foo{}))
}
