package pool

import (
	"github.com/pkg/errors"
	"reflect"
)

func is_nil(v interface{}) bool {
	if v == nil {
		return true
	}
	return reflect.ValueOf(v).IsNil()
}

func assign(target interface{}, source interface{}) error {
	rvt := reflect.ValueOf(target)
	rvs := reflect.ValueOf(source)

	if rvs.IsNil() {
		return errors.New("'source' parameter cannot be nil")
	}

	if rvt.IsNil() {
		return errors.New("'target' parameter cannot be nil (empty initialization is fine)")
	}

	if rvt.Type() != rvs.Type() {
		return errors.New("The types of source and target should be equal")
	}

	rvt.Elem().Set(rvs.Elem())

	return nil
}
