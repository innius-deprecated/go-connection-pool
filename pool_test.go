package pool

import (
	"errors"

	"github.com/stretchr/testify/assert"
	"sync/atomic"
	"testing"
)

type closer struct {
	Connection
	message string
	valid   bool
	cleanup func()
}

func (c *closer) IsValid() bool {
	return c.valid
}

func (c *closer) Close() error {
	c.cleanup()
	return nil
}

func (c *closer) setValid(b bool) {
	c.valid = b
}

func TestAutoCleanup(t *testing.T) {
	closec := int64(0)
	p := New(5, func() (Conn, error) {
		return &closer{
			message: "new()",
			cleanup: func() {
				atomic.AddInt64(&closec, 1)
			},
		}, nil
	})
	var closerr *closer = &closer{}

	assert.Nil(t, p.Get(closerr))
	assert.False(t, closerr.IsValid())
	assert.Equal(t, int64(0), closec)
	assert.Nil(t, closerr.Close())
	assert.Equal(t, int64(1), closec)

	p.Return(closerr)

	// even though we closed the resource manually, the pool calls close again because
	// it didn't know abouth that external call.
	assert.Nil(t, p.Get(closerr))
	assert.Equal(t, int64(2), closec)

	p.Return(closerr)

	assert.Nil(t, p.Get(closerr))
	assert.Equal(t, int64(3), closec)
}

func TestCompleteScenario(t *testing.T) {
	createc := int64(0)
	closec := int64(0)
	p := New(5, func() (Conn, error) {
		atomic.AddInt64(&createc, 1)
		return &closer{
			cleanup: func() {
				atomic.AddInt64(&closec, 1)
			},
		}, nil
	})
	assert.Equal(t, int64(0), createc)
	assert.Equal(t, int64(0), closec)

	var closerr *closer = &closer{}

	assert.Nil(t, p.Get(closerr))
	assert.False(t, closerr.IsValid())
	assert.Nil(t, closerr.Close())

	assert.Equal(t, int64(1), createc)
	assert.Equal(t, int64(1), closec)

	p.Return(closerr)

	assert.Equal(t, int64(1), createc)
	assert.Equal(t, int64(1), closec)

	var a *closer = &closer{}
	var b *closer = &closer{}

	assert.Nil(t, p.Get(a))
	assert.Nil(t, p.Get(b))

	assert.Equal(t, int64(3), createc)
	assert.Equal(t, int64(2), closec)
}

func TestLazyLoadEagerPoolConsumption(t *testing.T) {
	createc := int64(0)
	closec := int64(0)
	p := New(3, func() (Conn, error) {
		atomic.AddInt64(&createc, 1)
		return &closer{
			cleanup: func() {
				atomic.AddInt64(&closec, 1)
			},
		}, nil
	})
	assert.Equal(t, int64(0), createc)
	assert.Equal(t, int64(0), closec)

	var a *closer = &closer{}
	var b *closer = &closer{}
	var c *closer = &closer{}

	assert.Nil(t, p.Get(a))
	assert.Nil(t, p.Get(b))
	assert.Nil(t, p.Get(c))

	assert.Equal(t, int64(3), createc)
	assert.Equal(t, int64(0), closec)

	assert.False(t, a.IsValid())
	assert.False(t, b.IsValid())
	c.setValid(true)
	assert.True(t, c.IsValid())

	p.Return(a)
	p.Return(b)
	p.Return(c)

	var v *closer = &closer{}

	assert.Nil(t, p.Get(v))
	assert.True(t, v.IsValid())

	assert.Equal(t, int64(3), createc)
	assert.Equal(t, int64(2), closec)
}

func TestFailedDialWithRelease(t *testing.T) {
	var a *closer = &closer{}

	p := New(3, func() (Conn, error) {
		return nil, errors.New("failing dial")

	})
	assert.NotNil(t, p.Get(a))
	assert.Equal(t, 0, p.ActiveCount())
}

type failingcloser struct {
	Connection
	valid bool
}

func (c *failingcloser) IsValid() bool {
	return c.valid
}

func (c *failingcloser) Close() error {
	if c.valid {
		return nil
	}
	return errors.New("failing close")
}

func (c *failingcloser) setValid(b bool) {
	c.valid = b
}

func TestFailedCloseWithRelease(t *testing.T) {
	var a *failingcloser = &failingcloser{valid: true}

	p := New(1, func() (Conn, error) {
		return &failingcloser{}, nil

	})
	assert.Equal(t, 1, p.Size())
	assert.Nil(t, p.Get(a))
	assert.Equal(t, 1, p.ActiveCount())

	a.setValid(false)
	assert.Nil(t, p.Return(a))
	assert.Equal(t, 1, p.Size())
	assert.Equal(t, 0, p.ActiveCount())

	assert.NotNil(t, p.Get(a))
	assert.Equal(t, 1, p.Size())
	assert.Equal(t, 0, p.ActiveCount())
}
