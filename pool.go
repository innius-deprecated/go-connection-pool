package pool

import (
	"bitbucket.org/to-increase/go-config/types"
	"bitbucket.org/to-increase/go-tickets"
	"github.com/pkg/errors"
	"sync"
)

type pool struct {
	tickets   ticket.TicketPool
	excessive chan ticket.Ticket
	dial      DialFunc
	sessions  chan Conn
	closed    bool
	lock      sync.RWMutex
}

// Initialize a new dynamic pool with a given size and a dial func for generating new connections.
func New(size int, dial DialFunc) Pool {
	p := &pool{
		tickets:   ticket.NewTicketPool(size),
		dial:      dial,
		sessions:  make(chan Conn, size),
		excessive: make(chan ticket.Ticket, 0),
	}
	return p
}

// Initialize a new dynamic pool with a given size and a dial func for generating new connections.
// This will automatically subscribe to the dynamic value and update the queue size if its value changes.
func NewDynamic(size types.DynamicIntValue, dial DialFunc) Pool {
	s := size.Value()
	p := New(s, dial)
	size.OnChange(func(path types.Path, value int) {
		p.Resize(value)
	})
	return p
}

func (p *pool) Resize(desiredCapacity int) {
	p.lock.Lock() // Take a write lock to prevent modifications to this pool
	defer p.lock.Unlock()

	oldsize := p.tickets.Size()
	if !p.closed && oldsize != desiredCapacity {
		left := p.tickets.Size() - p.tickets.ActiveCount()
		p.tickets.Resize(desiredCapacity)
		newsize := p.tickets.Size()
		delta := newsize - oldsize // inverse, so a negative delta means a decrease.

		// Create a new channel
		oldsessions := p.sessions
		p.sessions = make(chan Conn, newsize)
		// We must first close the leftover sessions
		if delta < 0 {
			for delta != 0 {
				delta++
				left--
				select {
				case s := <-oldsessions:
					s.Close()
				default:
					// noop, we can have less sessions then our max size
				}

			}
		}
		// Are there any excessive tickets left?
		excessive := p.ActiveCount() - p.Size()
		if excessive > 0 {
			p.excessive = make(chan ticket.Ticket, excessive)
			for i := 0; i < excessive; i++ {
				p.excessive <- &struct{}{}
			}
			left = left - excessive
		} else {
			p.excessive = make(chan ticket.Ticket, 0)
		}

		// Then we can migrate
		for i := 0; i < left; i++ {
			select {
			case s := <-oldsessions:
				p.sessions <- s
			default:
				// noop, we don't necessarily have any connections left.
			}

		}
		close(oldsessions)
	}
}

func (p *pool) Close() error {
	p.lock.Lock()
	defer p.lock.Unlock()
	if p.closed {
		return errors.New("go-connection-pool: already closed")
	}
	p.closed = true

	var cumulative_error error = nil

	for t := p.tickets.GetTicket(); t != nil; t = p.tickets.GetTicket() {
		s := <-p.sessions
		cumulative_error = errors.Wrap(s.Close(), "Closing a connection failed.")
	}

	return cumulative_error
}

func (p *pool) Get(v Conn) error {
	p.lock.RLock()
	defer p.lock.RUnlock()
	var raw Conn = nil

	if p.closed {
		return errors.New("go-connection-pool: closed")
	}
	t := p.tickets.GetTicket()
	if t == nil {
		return errors.New("go-connection-pool: exhausted")
	}

retrieval:
	for {
		select {
		case s := <-p.sessions:
			if s.IsValid() {
				raw = s
				break retrieval
			} else {
				e := s.Close()
				// Only return if the session could not be closed.
				if e != nil {
					// return the ticket to the pool
					p.tickets.Return(&struct{}{})
					return errors.Wrap(e, "go-connection-pool: session became invalid, I could not close it.")
				}
			}
		default:
			// noop, exit the loop (no existing sessions left)
			break retrieval
		}
	}

	if raw == nil {
		r, err := p.dial()
		if err != nil {
			// return the ticket to the pool
			p.tickets.Return(&struct{}{})
			return errors.Wrap(err, "")
		}
		raw = r
	}
	return assign(v, raw)
}

func (p *pool) Return(v Conn) error {
	if is_nil(v) {
		return errors.New("go-connection-pool: provided pointer is nil, cannot return it to the pool")
	}
	p.lock.RLock()
	defer p.lock.RUnlock()
	if p.closed {
		return errors.Wrap(v.Close(), "go-connection-pool: pool is closed but can't close the resource.")
	} else {
		p.tickets.Return(&struct{}{})
		select {
		case _ = <-p.excessive:
			return v.Close()
		default:
			p.sessions <- v
		}
	}
	return nil
}

func (p *pool) ActiveCount() int {
	return p.tickets.ActiveCount()
}

func (p *pool) Size() int {
	return p.tickets.Size()
}

func (p *pool) PercentageUsed() int {
	return p.tickets.PercentageUsed()
}

func (p *pool) Capacity() int {
	p.lock.RLock()
	defer p.lock.RUnlock()
	if p.closed {
		return -1
	}
	return p.tickets.Size()
}
