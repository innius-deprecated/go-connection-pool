package pool

import "time"

// A function to 'dial' into a new connection. If error is nil, io.Closer MUST not be nill.
type DialFunc func() (Conn, error)

// A definition on the minimum set of capabilities a connection should have in order to be a pooled resource.
type Conn interface {
	// Close this connection
	Close() error
	// Is the resource represented still valid & usable?
	IsValid() bool

	// SetDeadline sets the read and write deadlines associated
	// with the connection. It is equivalent to calling both
	// SetReadDeadline and SetWriteDeadline.
	//
	// A deadline is an absolute time after which I/O operations
	// fail with a timeout (see type Error) instead of
	// blocking. The deadline applies to all future I/O, not just
	// the immediately following call to Read or Write.
	//
	// An idle timeout can be implemented by repeatedly extending
	// the deadline after successful Read or Write calls.
	//
	// A zero value for t means I/O operations will not time out.
	SetDeadline(t time.Time) error

	// SetReadDeadline sets the deadline for future Read calls.
	// A zero value for t means Read will not time out.
	SetReadDeadline(t time.Time) error

	// SetWriteDeadline sets the deadline for future Write calls.
	// Even if write times out, it may return n > 0, indicating that
	// some of the data was successfully written.
	// A zero value for t means Write will not time out.
	SetWriteDeadline(t time.Time) error
}

type Pool interface {
	// Close the whole connection pool. Any subsequent get() calls will fail
	Close() error

	// Get a connection. Provide a pointer to the type of connection you are requesting.
	// The caller is responsible for not mixing in two kind of types in the same connection pool.
	// Close this connection and return the underlying resource to the pool.
	// Any subsequent Close() calls return nil. Other operations will return errors.
	Get(Conn) error

	// Return a pooled resource back to the pool. It can then be given to other clients again.
	Return(Conn) error

	// The amount of connections currently in use.
	ActiveCount() int

	// The current size (capacity) of this pool
	Size() int

	// Request a resize of the current pool
	Resize(int)

	// The percentage of connections that's currently in use.
	PercentageUsed() int

	// The current capacity of this pool
	Capacity() int
}
