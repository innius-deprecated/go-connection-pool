package example

import (
	"bitbucket.org/to-increase/go-connection-pool"
	"github.com/stretchr/testify/assert"
	"testing"
)

const pseudo_query_res string = "a query result"

type foo struct {
	pool.Connection
	healthy bool
	query   string
}

func (f *foo) IsValid() bool {
	return f.healthy
}

func (f *foo) Close() error {
	f.healthy = false
	return nil
}

func (f *foo) Query() string {
	if f.IsValid() {
		return f.query
	} else {
		return ""
	}
}

func TestExampleUsage(t *testing.T) {
	pool_size := 2
	p := pool.New(pool_size, func() (pool.Conn, error) {
		return &foo{
			healthy: true,
			query:   pseudo_query_res,
		}, nil
	})
	assert.Equal(t, 0, p.ActiveCount())
	assert.Equal(t, pool_size, p.Capacity())

	// Just create a pointer to a default initialized type of connection you want to use.
	var a *foo = &foo{}
	var b *foo = &foo{}
	var c *foo = &foo{}

	// Ask for a connection from the pool and use it.
	assert.Nil(t, p.Get(a))
	assert.True(t, a.IsValid())
	assert.Equal(t, pseudo_query_res, a.Query())
	assert.Equal(t, 1, p.ActiveCount())

	// As soon as you're done, return it. Can also be done via defer p.Return(a) for convenience
	p.Return(a)
	assert.Equal(t, 0, p.ActiveCount())

	assert.Nil(t, p.Get(a))
	assert.Nil(t, p.Get(b))
	assert.NotNil(t, p.Get(c)) // Requesting 3 connections from a cap of 2 fails...

	// But resizing the capacity will allow the request to pass
	p.Resize(3)
	assert.Nil(t, p.Get(c))

	assert.True(t, c.IsValid())
	assert.Equal(t, pseudo_query_res, c.Query())

	p.Return(a)
	p.Return(b)
	p.Return(c)

	assert.Equal(t, 0, p.ActiveCount())
}

func TestCompleteScenario(t *testing.T) {
	p := pool.New(5, func() (pool.Conn, error) {
		return &foo{
			healthy: true,
			query:   pseudo_query_res,
		}, nil
	})
	assert.NotNil(t, p)
	assert.Equal(t, 0, p.ActiveCount())
	assert.Equal(t, 5, p.Capacity())

	var myfoo *foo = &foo{}

	assert.Nil(t, p.Get(myfoo))
	assert.True(t, myfoo.IsValid())
	assert.Equal(t, pseudo_query_res, myfoo.Query())
	assert.Equal(t, 1, p.ActiveCount())

	p.Return(myfoo)
	assert.Equal(t, 0, p.ActiveCount())
	assert.NotNil(t, myfoo.IsValid())

	assert.Nil(t, p.Get(myfoo))
	assert.True(t, myfoo.IsValid())
	assert.Equal(t, pseudo_query_res, myfoo.Query())
	assert.Equal(t, 1, p.ActiveCount())

	myfoo.Close()
	assert.False(t, myfoo.IsValid())
	assert.Equal(t, "", myfoo.Query())
	assert.Equal(t, 1, p.ActiveCount())

	p.Return(myfoo)
	assert.Equal(t, 0, p.ActiveCount())
	assert.NotNil(t, myfoo.IsValid())
}
