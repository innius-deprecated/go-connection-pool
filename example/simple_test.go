package example

import (
	"bitbucket.org/to-increase/go-connection-pool"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestSimpleScenario(t *testing.T) {
	p := pool.New(5, func() (pool.Conn, error) {
		return pool.NewConnection(), nil
	})
	assert.NotNil(t, p)
	assert.Equal(t, 0, p.ActiveCount())
	assert.Equal(t, 5, p.Capacity())

	var myfoo *pool.Connection = &pool.Connection{}

	e := p.Get(myfoo)
	assert.Nil(t, e)
	assert.True(t, myfoo.IsValid())
	assert.Equal(t, 1, p.ActiveCount())

	p.Return(myfoo)
	assert.Equal(t, 0, p.ActiveCount())
	assert.NotNil(t, myfoo.IsValid())
}

func TestSimpleScenarioWithClosedResource(t *testing.T) {
	p := pool.New(5, func() (pool.Conn, error) {
		return pool.NewConnection(), nil
	})
	assert.NotNil(t, p)
	assert.Equal(t, 0, p.ActiveCount())
	assert.Equal(t, 5, p.Capacity())

	var myfoo *pool.Connection = &pool.Connection{}

	e := p.Get(myfoo)
	assert.Nil(t, e)
	assert.True(t, myfoo.IsValid())
	assert.Equal(t, 1, p.ActiveCount())

	myfoo.Close()
	assert.False(t, myfoo.IsValid())
	assert.Equal(t, 1, p.ActiveCount())

	p.Return(myfoo)
	assert.Equal(t, 0, p.ActiveCount())
	assert.NotNil(t, myfoo.IsValid())
}
