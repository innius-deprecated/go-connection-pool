package example

import (
	"bitbucket.org/to-increase/go-connection-pool"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestExpiringConnections(t *testing.T) {
	exp := time.Millisecond * 100
	size := 2
	p := pool.New(size, func() (pool.Conn, error) {
		return pool.NewConnection(), nil
	})
	assert.Equal(t, 0, p.ActiveCount())

	var a *pool.Connection = &pool.Connection{}
	var b *pool.Connection = &pool.Connection{}
	var c *pool.Connection = &pool.Connection{}

	// I should be able to get 2 connections
	assert.Nil(t, p.Get(a))
	assert.Nil(t, p.Get(b))
	// But the third should fail (capacity is 2)
	assert.NotNil(t, p.Get(c))

	assert.Equal(t, 2, p.ActiveCount())
	assert.True(t, a.IsValid())
	assert.True(t, b.IsValid())
	assert.False(t, c.IsValid())

	// Returning B should invalidate it.
	p.Return(b)
	assert.Equal(t, 1, p.ActiveCount())

	// Getting a new second entry should be fine now.
	assert.Nil(t, p.Get(c))
	assert.Equal(t, 2, p.ActiveCount())

	// Wait until they are expired.
	a.SetDeadline(time.Now().Add(exp))
	c.SetDeadline(time.Now().Add(exp))
	time.Sleep(exp * 2)
	assert.False(t, a.IsValid())
	assert.False(t, c.IsValid())

	// We should be able to return them all.
	p.Return(a)
	p.Return(c)

	// Get a new one should work again. It should also be valid.
	assert.Equal(t, 0, p.ActiveCount())

	assert.Nil(t, p.Get(a))
	assert.True(t, a.IsValid())
	assert.Equal(t, 1, p.ActiveCount())
}
