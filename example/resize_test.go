package example

import (
	"testing"

	"bitbucket.org/to-increase/go-connection-pool"
	"github.com/stretchr/testify/assert"
)

// Initialize a pool
func initTestPool(initial_size int) pool.Pool {
	return pool.New(initial_size, func() (pool.Conn, error) {
		return pool.NewConnection(), nil
	})
}

func TestSimplePoolResize(t *testing.T) {
	p := initTestPool(5)

	assert.Equal(t, 0, p.ActiveCount())
	assert.Equal(t, 5, p.Capacity())

	p.Resize(1)
	assert.Equal(t, 0, p.ActiveCount())
	assert.Equal(t, 1, p.Capacity())

	p.Resize(3)
	assert.Equal(t, 0, p.ActiveCount())
	assert.Equal(t, 3, p.Capacity())
}

func TestPoolResizeScenario(t *testing.T) {
	p := initTestPool(2)

	a := &pool.Connection{}
	b := &pool.Connection{}

	ea := p.Get(a)
	assert.Nil(t, ea)
	assert.True(t, a.IsValid())
	assert.Equal(t, 1, p.ActiveCount())
	assert.Equal(t, 2, p.Capacity())

	// Resize the pool
	p.Resize(1)
	assert.Equal(t, 1, p.Capacity())

	// Getting the second session should fail
	eb := p.Get(b)
	assert.NotNil(t, eb)
	assert.Equal(t, 1, p.ActiveCount())

	// Return 1, none should be active
	p.Return(a)
	assert.Equal(t, 0, p.ActiveCount())

	// Resize the pool
	p.Resize(3)
	assert.Equal(t, 3, p.Capacity())

	// Retrieving 2
	ea = p.Get(a)
	eb = p.Get(b)
	assert.Nil(t, ea)
	assert.True(t, a.IsValid())
	assert.Nil(t, eb)
	assert.True(t, b.IsValid())

	assert.Equal(t, 2, p.ActiveCount())
	assert.Equal(t, 3, p.Capacity())
}

func TestPoolResizeShrinkWhileInUse(t *testing.T) {
	p := initTestPool(3)

	a := &pool.Connection{}
	b := &pool.Connection{}

	// Retrieving 2
	ea := p.Get(a)
	eb := p.Get(b)
	assert.Nil(t, ea)
	assert.True(t, a.IsValid())
	assert.Nil(t, eb)
	assert.True(t, b.IsValid())

	// 2 in use, don't kill existing connections but do decrease the capacity.
	p.Resize(1)

	assert.Equal(t, 2, p.ActiveCount())
	assert.Equal(t, 1, p.Capacity())

	p.Return(a)
	p.Return(b)

	assert.Equal(t, 0, p.ActiveCount())
	assert.Equal(t, 1, p.Capacity())
}
