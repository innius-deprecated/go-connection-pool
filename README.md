# go-connection-pool

A lazy and dynamically resizable connection pool. The example folder serves both as as an example as well as tests.

## Usage
See the 'example' folder for examples. A good example on how to create your own connection can be found in [the full_test.go example](example/full_test.go).
