package pool

import (
	"github.com/pkg/errors"
	"time"
)

// Factory function for creating a new connection
func NewConnection() *Connection {
	return &Connection{
		open: true,
	}
}

// A convenience type implementing the basic behaviour of a connection.
// You can embed this struct in your own connection.
type Connection struct {
	writeDeadline *time.Timer
	readDeadline  *time.Timer
	open          bool
}

func (c *Connection) Close() error {
	c.open = false
	return nil
}

func (c *Connection) IsValid() bool {
	return c.open
}

func (c *Connection) SetDeadline(t time.Time) error {
	e := c.SetReadDeadline(t)
	if e != nil {
		return errors.Wrap(e, "Can't configure ReadDeadline in SetDeadline")
	}

	e = c.SetWriteDeadline(t)
	if e != nil {
		return errors.Wrap(e, "Can't configure WriteDeadline in SetDeadline")
	}

	return nil
}

func (c *Connection) SetReadDeadline(t time.Time) error {
	if !c.IsValid() {
		return errors.New("Can't set a read deadline: connection is not valid.")
	}
	duration := t.Sub(time.Now())
	if c.readDeadline == nil {
		c.readDeadline = time.NewTimer(duration)
		go func() {
			<-c.readDeadline.C
			c.Close()
		}()
	} else {
		if !c.readDeadline.Reset(duration) {
			return errors.New("The read deadline timer alread expired")
		}
	}
	return nil
}

func (c *Connection) SetWriteDeadline(t time.Time) error {
	if !c.IsValid() {
		return errors.New("Can't set a write deadline: connection is not valid.")
	}
	duration := t.Sub(time.Now())
	if c.writeDeadline == nil {
		c.writeDeadline = time.NewTimer(duration)
		go func() {
			<-c.writeDeadline.C
			c.Close()
		}()
	} else {
		if !c.writeDeadline.Reset(duration) {
			return errors.New("The write deadline timer alread expired")
		}
	}
	return nil
}
